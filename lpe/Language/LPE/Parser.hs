{-# LANGUAGE GADTs #-}
module Language.LPE.Parser where

import Control.Applicative
import Data.Text.Lazy
import Text.Trifecta

data Expr where
    Symbol      :: Text -> Expr
    Abstraction :: Expr -> Expr -> Expr
    Application :: Expr -> Expr -> Expr
  deriving (Show)

parseExpr :: Parser Expr
parseExpr = parseSymbol <|> parseLambda <|> parseApplication

parseSymbol :: Parser Expr
parseSymbol = Symbol . pack <$> delimited (many alphaNum)
  where
   delimited = between (symbolic '|') (symbolic '|')

parseLambda :: Parser Expr
parseLambda = Abstraction <$> delimited parseExpr <*> parseExpr
  where
    delimited = (>>) $ between (symbolic '(') (symbolic ')') (symbol "lambda")

parseApplication :: Parser Expr
parseApplication = Application <$> delimited parseExpr <*> parseExpr
  where
    delimited = between (symbolic '(') (symbolic ')')
